import { Cliente } from '../../cliente/model/cliente';
import { Veiculo } from '../../veiculos/model/veiculo';
import { StatusPagamento } from '../../entity/status-pagamento';

export class Aluguel {

    dataAluguel: string;
    dataDevolucao: string;
    valorTotal: number;
    kmPecorrido: number;
    cliente: Cliente;
    veiculo: Veiculo;
    statusPagamento: StatusPagamento;

    constructor() {
        this.dataAluguel = null;
        this.dataDevolucao = null;
        this.valorTotal = 0;
        this.kmPecorrido = 0;
        this.cliente = new Cliente;
        this.veiculo = new Veiculo;
        this.statusPagamento = new StatusPagamento;
    }
}
